#!/bin/bash

image_prefix=192.168.1.5:5000/

dotnet publish -c Release
docker build \
    --build-arg NODE_ENV=$NODE_ENV \
    -t ${image_prefix}hello-app:$GO_PIPELINE_LABEL .
